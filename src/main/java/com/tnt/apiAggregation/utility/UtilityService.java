package com.tnt.apiAggregation.utility;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class UtilityService {
	public Set<String> generateSet(String reqStr) {
		if(reqStr==null) {
			return null;
		}
		Set<String> set = new HashSet<String>();
		String[] strArray = reqStr.split(",");
		for (String str : strArray) {
			set.add(str);
		}
		return set;
	}
}
