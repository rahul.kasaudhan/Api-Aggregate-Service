package com.tnt.apiAggregation.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.tnt.apiAggregation.dto.ResponseDto;
import com.tnt.apiAggregation.externalGateway.ExternalAPIGateway;
import com.tnt.apiAggregation.utility.UtilityService;

@Service
public class AggregationServiceImpl implements AggregationService{
	
	private static final Logger log = LoggerFactory.getLogger(AggregationServiceImpl.class);
	
	@Autowired
	ExternalAPIGateway externalAPIGateway;

	@Autowired
	UtilityService utilityService;
	
	@Value("${response.completion.time}")
	private long maxAllowedRespTime;
	
	@Override
	public ResponseDto buidAggResponse(String pricingCountryCodes, String trackOrderNumbers, String shipmentOrderNumbers)
			throws InterruptedException, ExecutionException {
		
		ResponseDto aggregatedResponse = new ResponseDto();
		ExecutorService executor = Executors.newFixedThreadPool(3);
		
		
		Set<String> pricingCountryCodesSet = utilityService.generateSet(pricingCountryCodes);
		Set<String> trackOrderNumbersSet = utilityService.generateSet(trackOrderNumbers);
		Set<String> shipmentOrderNumbersSet = utilityService.generateSet(shipmentOrderNumbers);
		
		Callable<Map<String, BigDecimal>>  task1=() -> {
			Map<String, BigDecimal> pricingResult = generatePricingResp(pricingCountryCodesSet);
			return pricingResult;
		};
		
		Callable<Map<String, String>>  task2=() -> {
			Map<String, String> trackingResult = generateTrackingResp(trackOrderNumbersSet);
			return trackingResult;
		};
		
		Callable<Map<String, List<String>>>  task3=() -> {
			Map<String, List<String>> shipmentResult = generateShipmentResp(shipmentOrderNumbersSet);
			return shipmentResult;
		};
		
		Future<Map<String, BigDecimal>> pricingFuture=null;
		Future<Map<String, String>> trackFuture=null;
		Future<Map<String, List<String>>> shipmentFuture=null;
		
		pricingFuture = executor.submit(task1);
		trackFuture = executor.submit(task2);
		shipmentFuture = executor.submit(task3);
		
		Map<String, BigDecimal> pricingResult = pricingFuture.get();
		Map<String, String> trackResult = trackFuture.get();
		Map<String, List<String>> shipmentResult = shipmentFuture.get();
		
		/*
		 * to avoid extra backslash and double qoutes in respose 
		 */
		Map<String, String> trackRes = new HashMap<String,String>();
		for(String key:trackResult.keySet()){
			String value = new String(trackResult.get(key));
			value = value.trim();
			value = value.substring(1,value.length() - 1);
			value = value.replace("\\","");
			value= value.translateEscapes();
			trackRes.put(key, value);
		}
		
		aggregatedResponse.setPricing(pricingResult);
		aggregatedResponse.setTrack(trackRes);
		aggregatedResponse.setShipments(shipmentResult);
		executor.shutdown();
		log.info("All Tasks responses Aggregated!!");		
		return aggregatedResponse;
	}
	
	private Map<String, BigDecimal> generatePricingResp(Set<String> pricingCountryCodes) {
		log.info("Task for generating pricing response Started: " + pricingCountryCodes);
		Map<String, BigDecimal> responseMap = new HashMap<String, BigDecimal>();
		if(pricingCountryCodes==null) {
			return responseMap; 
		}
		ExecutorService executor = Executors.newSingleThreadExecutor();
		try {	
			long start = System.currentTimeMillis();
			long end = start + maxAllowedRespTime;
			
			for(String pricingCC:pricingCountryCodes) {
				Callable<BigDecimal> task=() -> {
					BigDecimal res=externalAPIGateway.getPricingResponse(pricingCC);
					return res;
				};
				Future<BigDecimal> future = executor.submit(task);
				BigDecimal result = future.get(end - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
				if(result!=null) {
					responseMap.put(pricingCC, result);
				}
			}
		}
		catch(Exception e) {
			log.error("Exception Occurred in calling external pricing API : {}",e.getMessage());
		}
		executor.shutdown();
		log.info("Task for generating pricing response Completed!!");
		return responseMap;
	}
	
	private Map<String, String> generateTrackingResp(Set<String> trackOrderNumbers)
			throws InterruptedException {
		log.info("Task for generating Tracking response Started: {}" + trackOrderNumbers);
		Map<String, String> responseMap = new HashMap<String, String>();
		if(trackOrderNumbers==null) {
			return responseMap; 
		}
		ExecutorService executor = Executors.newSingleThreadExecutor();
		try {	
			long start = System.currentTimeMillis();
			long end = start + maxAllowedRespTime;
			
			for(String trackON:trackOrderNumbers) {
				Callable<String> task=() -> {
					String res=externalAPIGateway.getTrackResponse(trackON);
					return res;
				};
				Future<String> future = executor.submit(task);
				String result = future.get(end - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
				if(result!=null) {
					responseMap.put(trackON, result);
				}
			}
		}
		catch(Exception e) {
			log.error("Exception Occurred in calling external track API : {}",e.getMessage());
		}
		executor.shutdown();
		log.info("Task for generating Tracking response Completed!!");
		return responseMap;
	}
	  
	private	 Map<String, List<String>> generateShipmentResp(Set<String> shipmentOrderNumbers)
			throws InterruptedException {
		log.info("Task for generating shipment response Sarted: {}" + shipmentOrderNumbers);
		Map<String, List<String>> responseMap = new HashMap<String, List<String>>();
		if(shipmentOrderNumbers==null) {
			return responseMap; 
		}
		ExecutorService executor = Executors.newSingleThreadExecutor();
		try {	
			long start = System.currentTimeMillis();
			long end = start + maxAllowedRespTime;
			
			for(String shipmentON:shipmentOrderNumbers) {
				Callable<List<String>> task=() -> {
					List<String> res=externalAPIGateway.getShipmentsResponse(shipmentON);
					return res;
				};
				Future<List<String>> future = executor.submit(task);
				List<String> result = future.get(end - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
				if(result!=null){
					responseMap.put(shipmentON, result);
				}
			}
		}
		catch(Exception e) {
			log.error("Exception Occurred in calling external shipments API : {}",e.getMessage());
		}
		executor.shutdown();
		log.info("Task for generating shipment response Completed");
		return responseMap;
	}

}
