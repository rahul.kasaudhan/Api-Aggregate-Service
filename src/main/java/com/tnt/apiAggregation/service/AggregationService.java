package com.tnt.apiAggregation.service;

import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import com.tnt.apiAggregation.dto.ResponseDto;

@Service
public interface AggregationService {

	public ResponseDto buidAggResponse(String pricingCountryCodes, String trackOrderNumbers, String shipmentOrderNumbers)
			throws InterruptedException, ExecutionException;
	
}
