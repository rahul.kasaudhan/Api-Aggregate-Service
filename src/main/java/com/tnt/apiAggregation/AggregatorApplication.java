package com.tnt.apiAggregation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;


@SpringBootApplication
@EnableHystrix
public class AggregatorApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AggregatorApplication.class, args);
	}
	
}
